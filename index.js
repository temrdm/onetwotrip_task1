#!/usr/bin/env node
const redis = require('redis')
const settings = require('./settings')

const connection = redis.createClient(settings.DB_CONNECTION)

const logger = Logger(settings.LOG_LEVEL)

main()

function main() {
    if (process.argv.length < 3) {
        return selectMode()
    }

    const argument = process.argv[2]

    if (argument === '--get-errors' || argument === 'getErrors') {
        return getErrors()
    }
}

function selectMode() {
    return setLock()
        .then(setMessagesSenderMode)
        .catch(setMessagesReaderMode)
}

function setMessagesSenderMode() {
    logger.log('MessagesSenderMode')

    const extendLockInterval = setInterval(extendLock, settings.LOCK_EXTENDS_IN)
    const sendMessageInterval = setInterval(sendMessage, settings.SEND_MESSAGE_IN)

    function sendMessage() {
        const msg = makeRandomMessage()

        connection.rpush(settings.MESSAGES_POOL, msg, (error, result) => {
            if (error) {
                logger.error('Send message error', error)
            }

            if (!result || error) {
                return errorHandler()
            }

            logger.log('Message sent: ' + msg)
        })
    }

    function makeRandomMessage() {
        return new Date().getTime()
    }

    function extendLock() {
        connection.pexpire(settings.LOCK_KEY, settings.LOCK_EXPIRES_IN, (error, result) => {
            if (error) {
                logger.error('Extend lock error', error)
            }

            if (!result || error) {
                errorHandler()
            }
        })
    }

    function clearLocalIntervals() {
        clearInterval(extendLockInterval)
        clearInterval(sendMessageInterval)
    }

    function errorHandler() {
        clearLocalIntervals()
        setTimeout(() => selectMode(), settings.RETRY_IN)
    }
}

function setMessagesReaderMode() {
    logger.log('MessagesReaderMod')

    const readMessageIntervalId = setInterval(readMessage, settings.READ_MESSAGE_IN)
    const setLockIntervalId = setInterval(setLockIntervalCallback, settings.LOCK_EXPIRES_IN)

    function readMessage() {
        connection.lpop(settings.MESSAGES_POOL, (error, result) => {
            if (error) {
                logger.error('Read message error', error)
                errorHandler()
            }

            if (result) {
                readMessageHandler(result)
            }
        })
    }

    function readMessageHandler(msg) {
        if (isMessageIncorrect(msg)) {
            logger.info('New incorrect message: ', msg)

            connection.rpush(settings.ERROR_POOL, msg, (error, result) => {
                if (error) {
                    logger.error('Store error message error', error)
                }

                if (error || !result) {
                    errorHandler()
                }
            })
        }

        logger.log('Message read: ', msg)
    }

    function isMessageIncorrect(msg) {
        return Math.random() >= settings.CORRECT_MESSAGE_CHANCE
    }

    function setLockIntervalCallback() {
        return setLock()
            .then(lockSetSuccess)
            .catch(() => false)
    }

    function lockSetSuccess() {
        clearLocalIntervals()
        setMessagesSenderMode()
    }

    function clearLocalIntervals() {
        clearInterval(readMessageIntervalId)
        clearInterval(setLockIntervalId)
    }

    function errorHandler() {
        clearLocalIntervals()
        setTimeout(() => selectMode(), settings.RETRY_IN)
    }
}

function setLock() {
    return new Promise((resolve, reject) => {
        return connection.set(settings.LOCK_KEY, settings.LOCK_VALUE, 'NX', 'PX', settings.LOCK_EXPIRES_IN, callback)

        function callback(error, result) {
            if (error) {
                logger.error('Set lock error', error)
            }

            return error || !result ? reject(error) : resolve(result)
        }
    })
}

function getErrors() {
    connection.watch(settings.ERROR_POOL)
    connection.multi()
        .lrange(settings.ERROR_POOL, 0, -1)
        .del(settings.ERROR_POOL)
        .exec((error, [errorsList]) => {
            if (error) {
                logger.error('Error pool reading problem', error)
            } else {
                console.log(errorsList.join(', '))
            }
            connection.quit()
            process.exit(-1)
        })
}

function Logger(logLevel) {
    return {
        log,
        error,
        info,
    }

    function log() {
        if (logLevel > 2) {
            console.log.apply(console, arguments)
        }
    }

    function info() {
        if (logLevel > 1) {
            console.info.apply(console, arguments)
        }
    }

    function error() {
        if (logLevel > 0) {
            console.error.apply(console, arguments)
        }
    }
}
