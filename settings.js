module.exports = {
    DB_CONNECTION: {},
    LOG_LEVEL: 3, // SILENT = 0, ERROR = 1, INFO = 2, LOG = 3
    MESSAGES_POOL: 'messages_pool',
    ERROR_POOL: 'error_pool',
    LOCK_KEY: 'lock.app',
    LOCK_VALUE: 'lock.value',
    SEND_MESSAGE_IN: 500,
    READ_MESSAGE_IN: 500,
    LOCK_EXPIRES_IN: 1000,
    LOCK_EXTENDS_IN: 800,
    CORRECT_MESSAGE_CHANCE: 0.95,
    RETRY_IN: 1000,
}
